begin
    
    require_relative("gubg.build/bootstrap.rb")
rescue LoadError
    puts("This seems to be a fresh clone: I will update all required submodules for you.")
    sh "git submodule update --init"
    sh "rake uth prepare"
    retry
end

require("gubg/shared")
require("gubg/cook/instance")
require("gubg/cook")

gubg_submods = %w[build std math io algo].map{|e| "gubg.#{e}"}
namespace :gubg do
    task :prepare do
        GUBG::each_submod(submods: gubg_submods) { |info| sh "rake prepare" }
    end
    task :clean do
        GUBG::each_submod(submods: gubg_submods) { |info| sh "rake clean" }
    end
    task :proper do
        GUBG::each_submod(submods: gubg_submods) { |info| sh "rake proper" }
    end
end

desc "Prepare the submods"
task :prepare do
    Rake::Task["gubg:prepare"].invoke
end

desc "Update all to head"
task :uth do
    GUBG::each_submod() do |info|
        sh "git checkout #{info[:branch]}"
        sh "git pull --rebase"
        sh "rake uth" if (info[:name][/^gubg\./] and `rake -AT`["rake uth"])
    end
end

task :list do
    rcps = GUBG::Cook[:default].naft.recipes
    
    puts "Available recipes"
    rcps.each do |uri, v|
        puts " - #{uri} (Type:#{v.type})"
    end
end

task :build, [:uri_or_glob] do |t,arg|
    rcps = GUBG::Cook[:default].build(arg[:uri_or_glob])
    
    max_uri_length = rcps.map { |uri, fn| uri.length }.max
    puts "Build the following recipe(s):"
    rcps.each do |uri, fn|
        puts " - #{uri} " + (" "*(max_uri_length - uri.length)) + ": #{fn}" 
    end
end

["doc"].each { |d| load File.join(d, "rakefile.rb") }


task :test do
    cook.naft()
end
