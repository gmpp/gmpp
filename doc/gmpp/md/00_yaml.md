---
amsthm:
  plain:	            [Theorem]
  plain-unnumbered:	    [Lemma, Corollary]
  definition:	            [Definition, Proposition, Conjecture, Example, Postulate, Problem]
  definition-unnumbered:    []
  remark:	            [Case]
  remark-unnumbered:	    [Remark, Note]
  proof:	            [proof]
  parentcounter:	    chapter
  unofficial-use:	    [flushright]
title:	        GMPP Technical documentation
author:	        Thomas Fannes
date:	        \today
keywords:	gmpp, graph, minig
toc-depth:	5
lang:	        en
documentclass:	book
classoption:	oneside, article
colorlinks:	true
cref:           true
...
 
