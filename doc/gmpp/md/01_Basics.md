

# Graphs


## Graphs notation

<div class="Definition" id="def:graph" name="Graph">
A graph $G$ is an ordered pair $(V,E)$ where

 * $V$ is called the the vertex set, and,
 * $E$ is a set of 2-element subsets of $V$ called the edge set.

For a graph $G$, we use $V(G)$ and $E(G)$ to denote the vertex and edge sets. 
</div>

Let $G$ be a graph. Two vertices $u$ and $v$ are *adjacent* in $G$ if there is an edge $e = \{u,v\} \in E(G)$. This edge $u$ is *incident* to $u$ and $v$. A *path $P$* in $G$ is sequence of vertices such that any two consecutive vertices are adjacent, i..e., they form an edge. The length of a path is the number of vertices in the sequence minus one, and thus, equal to the number of edges represented by that path. Two vertices $u$ and $v$ are *connected* in $G$ if there exists a path starting at $u$ and ending in $v$. The property of *connectedness* is an equivalence relation, i.e.,
 
 * **reflexive**: There is a trivial path of length zero from any vertex to itself.
 * **symmetric**: If $u$ is connected to $v$, then $v$ is connected to $u$.
 * **transitive**: If $u$ is connected to $v$ and $v$ to $w$ then there is path connecting $u$ to $w$.

A vertex set $S$ is *internally connected* if any two distinct vertices are connected by a path $P$ which resides in $S$, i.e., $P \subset S$. A *connected component* of a graph $G$ is a internally connected vertex set $V \subset V(G)$ such that no vertex of $V$ is connected to a vertex of $V(G) \setminus V$.

For a vertex $u$, its neighborhood in $G$ is defined as the set of all vertices adjacent tot $u$, or,
$$ 
\eta_G(u) = \{ v | \{u,v\} \in E(G) \}.
$$

For a vertex subset $S \subset V(G)$, the neighborhood of $S$ in $G$ are those vertices adjacent to at least a vertex in $S$, but no in $S$, i.e.,
$$
\eta_G(S) = \bigcup_{v \in S} \eta_G(v) \setminus S.
$$

## Subgraph

<div class="Definition" id="def:subgraph" name="Subgraph">
Let $G$ be a graph. A graph $H$ is a *subgraph* of $G$ (and $G$ a supergraph of $H$) if both the vertex set and edge set are subsets, i.e., 

 * $V(H) \subset V(G)$, and 
 * $E(H) \subset E(G)$. 

</div>


<div class="Definition" id="def:inducedsubgraph" name="Induced subgraph">
Let $G$ be a graph and $S \subset V(G)$. $H$ is the *induced subgraph* of $G$ on $S$ if
	
 * $V(H) = S$, and,
 * $E(H) = \{\{u,v\} \subset S | \{u,v\} \in E(G) \}$.
</div>

Let $G$ be a graph and $S$ a vertex set of $G$. Then $G \setminus S$ is the induced graph of $G$ on $V(G)\setminus S$, i.e., all the vertices from $G$ expect $S$.

## Graph properties

 * A graph $G$ is *complete* if any two distinct vertices are adjacent. 
 * An *$n$-clique* in a graph $G$ is a vertex subset of size $n$ where any two distinct vertices are adjacent in $G$. 
 * A graph is *connected* if any two distinct vertices are connected by a path.  Note that, according to the definition, a graph containing a single vertex (and no edge) is connnected.

 

