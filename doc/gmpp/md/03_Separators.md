# Separators

## Connectedness-respecting contraction (CRC)

<div class="Definition" id="def:crc" name="Connectedness-respecting contraction">
Let $G$ be a graph and $S \subset V(G)$ an internally connected vertex set. The Connectedness-respecting contraction (CRC) of $S$ w.r.t. $G$, denoted $\kappa_G(S)$, is the graph $H$, with

 * $V(H) = S \cup \eta_G(S)$, and,
 * $E(H) = \{ \{u,v\} \subset V(H) \mid \{u,v\} \in E(G) \text{ or } u \text{ and } v \text{ are connected in }G \setminus S\}$

</div>

Let $u$ and $v$ be any two vertices of $H$, and let us consider whether $u$ and $v$ are adjacent in $H$ or not. 
First of all, if $u$ and $v$ are adjacent in $G$, then they are adjacent in $H$. The second part states that $u$ and $v$ are adjacent if they are connected in $G \setminus S$. This implies that both $u$ and $v$ should reside in $\eta_G(S)$ and thus not in $S$.

<div class="Proposition" id="prop:crc_connected" name="CRC and connectedness">
Let $S$ be an internally connected vertex set in $G$, $H = \kappa_G(S)$ and $T \subset V(H)$. Any two vertices of $H$ are connected in $H \setminus T$ if, and only if, they are connected in $G \setminus T$.
</div>

The above proposition clarifies the name of the CRC: the connectedness for vertices is respected
by the contraction.

<div class="proof">
$(\Rightarrow)$. 
Let us consider two vertices of $H$, connected in $H \setminus T$. This implies the existence of a path $P$ connecting both vertices, and $P$ contains not vertices of $T$.

Let $u$ and $v$ be two consecutive vertices of that path $P$, and thus $\{u,v\}$ is an edge of $H$. Following \Cref{def:sep_trans}, either $\{u,v\}$ is an edge in $H$, or $u$ and $v$ are connected in $G \setminus S$. As $T \subset V(H)$ and both $u$ and $v$ are not elements of $T$, this implies that $u$ and $v$ are connected in $G \setminus T$.

$(\Leftarrow)$. Consider two vertices $u$ and $v$ of $H$, connected in $G \setminus T$ by a path $P$. We will construct a path $P^\prime$ from $P$ as follows. 

Take any vertex along $P$ which does not reside in $H$, and let $j$ be its index. Take $i$ the largest value smaller than $j$ such that $P_i \in V(H)$, and $k$ the smallers value larger than $j$ such that $P_k \in V(H)$. 

First of all, if such a vertex $P_j$ exists, then also $P_i$ and $P_k$ exists, as $u$ and $v$ are both in $H$. Secondly, $P_i$ resides in $\eta_G{S}$ and not in $S$, as this would imply that  $P_{i+1}$ resides in $\eta_G{S}$ and thus in $V(H)$. Similary for $P_k$. Finally, as $P[i:k]$ is a path in $G \setminus S$, we know that $P_i$ and $P_k$ are adjacent in $H$.

Hence, we can construct $P^\prime$ based on $P$ by removing all vertices that do not reside in $H$ and $P^\prime$ will be a path connecting $u$ and $v$ in $H$. As $P^\prime$ has not vertices of $T$, $u$ and $v$ are connected in $H \setminus T$.
</div>

## Separators


<div class="Definition" name="Separation transformation">
Let $G$ be a connected graph, $S \subset V(G)$ a vertex subset, and $\mathcal{D}$ the set of connected components in $G \setminus S$. The separation transformation of $S$ w.r.t. $G$, denoted $\varphi_G(S)$, is defined as 

$$
\varphi_G(S) = \{ \kappa_G(V) \, \mid \, V \in \mathcal{D} \}.
$$
</div>
Take $V \in \mathcal{D}$ as in the above definition. Then any such connected component is also an internally connected set in $G$. Furthermore, the neighborhood of $V$ in $G$ is a subset of $S$.

<div class="Definition" name="Separators and minimal separators">
A vertex subset $S$ of a connnected graph $G$ is a separator for $G$ if $G \setminus S$ has at least two connected components.

A separator $S$ is a minimal separator for $G$ if there are two vertices $u,v \in V(G) \setminus S$ disconnected in $G\setminus S$ but no in $G \setminus S^\prime$ for $S^\prime \subsetneq S$.
</div>


<div class="Proposition" name="Minimal separators (alternative)">
Let $G$ be a connected graph. $S \subset V(G)$ is a minimal separator for $G$ if, and only if, $\varphi_G(S)$ contains two distinct graphs $H$ with $S \subset V(H)$.
</div>

<div class="proof">
$(\Rightarrow).$ Let $S$ be a minimal separator for $G$, and let $u$ and $v$ be vertices of $V(G) \setminus S$ which are disconnected in $G\setminus S$, but not in $G \setminus S^\prime$ with $S^\prime \subsetneq S$. 
</div>

<div class="Proposition" name="The number of subgraph vertices">
Let $G$ be a connected graph. Then 
$$
\left| \bigcup_{S \subset V(G)} \varphi_G(S) \right| = O(n^k), \quad \text{ for }|S| \leq k.
$$
</div>

<div class="proof">
Let $G$ be any graph with $|V(G)| = n$ vertices. Throughout this proof, we use $f(n,k)$ to denote
$$
f(n,k) = \left| \bigcup_{S \subset V(G)} \varphi_G(S) \right|, 
$$
for $|S| = k$. 

**The recursive formula**




</div>
