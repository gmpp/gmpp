# Minimal separator tree decomposer (MSTD)

## Structure of the graph

We construct a DAG with three types of vertices: component, separator and subgraph vertices. 

## Subgraph vertex

A subgraph vertex is a tuple $(S,A)$ of two vertex sets where $S$ represent *separator* vertices and $A$ the *active* vertices. By construction, $A$ will be an internally connected component in $G$, and $S = \eta_G(A)$


## The number of vertices

### The number of separator vertices

We consider separators up to size $k$, and thus there are maximally $ON(n^k)$ separator vertices.

### The number of subgraph vertices


