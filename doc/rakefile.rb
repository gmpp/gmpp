def make_doc(dir: nil, vars: {}, output: nil, mask: nil)
    doc_dir = File.dirname(__FILE__)
    raise "Please supply a dir" unless dir
    raise "Please supply an output file" unless output
    mask = mask || "" 

    fns = Dir[File.join(doc_dir, dir, "md", "*.md")].select do |fn|
        fn.include?(mask)
    end.to_a.sort { |x,y| x <=> y }
    template_fn = File.join(doc_dir, "template", "template.tex")
    filter_fn = File.join(doc_dir, "extern", "pandoc-amsthm", "bin", "pandoc-amsthm.py")
    args = [
        "pandoc", 
        "-N",
        "--from=markdown+fenced_code_attributes",
        "--template=#{template_fn}",
        "--pdf-engine=xelatex", 
        "--filter=pandoc-crossref", 
        "--filter=#{filter_fn}", 
        "--toc", 
        "-o", File.join(doc_dir, output)]
    args += fns
    vars.each do |k,v| 
        args += ["--variable", "#{k}=#{v}"] 
    end
    sh(*args)

end


desc "make pdf documentation"
task :pdf, [:mask] do |t, args|
    version = "gmpp.v1"
    make_doc(dir: "gmpp", vars: {footer: version}, output: "#{version}.pdf", mask: args[:mask])
end
